const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
    template: './src/index.html',
    filename: '../index.html',
    inject: 'body',
});

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve('dist/static'),
        filename: 'index_bundle.js'
    },
    module: {
        rules: [
            {test: /\.js$/, loader: 'babel-loader', exclude: [/node_modules/, /dist/]},
            {test: /\.jsx$/, loader: 'babel-loader', exclude: /node_modules/},
            {test: /\.(eot|ttf|woff|woff2)(\?v=\d+\.\d+\.\d+)?$/, loader: 'file-loader'},
            {test: /\.(css|scss)$/, use: ['style-loader', 'css-loader']}
        ]
    },
    plugins: [HtmlWebpackPluginConfig]
}
