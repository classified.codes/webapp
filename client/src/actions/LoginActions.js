import * as localStorage from '../localStorage.js';
import {fetchBlob} from './AccountActions.js';
import {identityCheck, deriveAESKey} from './common.js';

export function login(email, password) {
    return (dispatch, getState) => {
        if (!email) {
            dispatch({type: 'LOGIN_EMAIL_ERROR', payload: 'You cannot leave this blank'});
        } else {
            dispatch({type: 'LOGIN_EMAIL_ERROR', payload: ''});
        }
        if (!password) {
            dispatch({type: 'LOGIN_PASSWORD_ERROR', payload: 'You cannot leave this blank'});
        } else {
            dispatch({type: 'LOGIN_PASSWORD_ERROR', payload: ''});
        }
        if (!email || !password) {
            return new Promise((resolve, reject) => reject());
        }
        return dispatch(deriveAESKey(email, password))
        .then(() => {
            return dispatch(identityCheck(email, getState().main.AESKey));
        })
        .then(() => {
            return dispatch(fetchBlob(getState().main.Identity, getState().main.AESKey));
        })
        .catch(() => {
            dispatch({type: 'LOGIN_ERROR', payload: 'Incorrect username or password'});
            return new Promise((resolve, reject) => reject());
        });
    };
}

export function autoLogin(email, key) {
    return (dispatch, getState) => {
        dispatch({type: 'DERIEVE_KEY_FULFILLED', payload: key});
        return dispatch(identityCheck(email, key))
        .then(() => {
            return dispatch(fetchBlob(getState().main.Identity, getState().main.AESKey));
        });
    };
}

export function signOut() {
    localStorage.clear();
    return {
        type: 'SIGN_OUT'
    }
}
