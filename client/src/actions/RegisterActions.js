import * as PasswordManager from '../crypto/PasswordManager.js';
import {saveBlob} from './AccountActions.js';
import {identityCheck, deriveAESKey} from './common.js';

export function register(email, password) {
    return (dispatch, getState) => {
        return dispatch(deriveAESKey(email, password))
        .then(() => {
            return dispatch(createAccount(email, getState().main.AESKey));
        })
        .then(() => {
            return dispatch(identityCheck(email, getState().main.AESKey));
        })
        .then(() => {
            return dispatch(saveBlob());
        });
    };
}

function createAccount(email, aesKey) {
    return dispatch => {
        dispatch({type: 'REGISTER_PENDING'});
        return PasswordManager.register(email, aesKey)
        .then(() => {
            dispatch({type: 'REGISTER_FULFILLED'});
        });
    };
}

