import * as PasswordManager from '../crypto/PasswordManager.js';

export function deriveAESKey(email, password) {
    return dispatch => {
        dispatch({type: 'DERIEVE_KEY_PENDING'});
        return PasswordManager.deriveAESKey(email, password)
        .then((AESKey) => {
            dispatch({type: 'DERIEVE_KEY_FULFILLED', payload: AESKey});
        });
    };
}

export function identityCheck(email, aesKey) {
    return dispatch => {
        dispatch({type: 'IDENTITY_CHECK_PENDING'});
        return PasswordManager.identityCheck(email, aesKey)
        .then((identity) => {
            dispatch({type: 'IDENTITY_CHECK_FULFILLED', payload: identity});
        });
    };
}
