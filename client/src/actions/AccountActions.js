import * as PasswordManager from '../crypto/PasswordManager.js';

export function addPassword(password) {
    return (dispatch, getState) => {
        dispatch({
            type: 'ADDED_PASSWORD',
            payload: password
        })
        dispatch(saveBlob());
    };
}

export function updatePassword(password, index) {
    return (dispatch, getState) => {
        dispatch({
            type: 'UPDATED_PASSWORD',
            payload: password,
            index: index
        })
        dispatch(saveBlob());
    };
}

export function deletePassword(index) {
    return (dispatch, getState) => {
        dispatch({
            type: 'DELETE_PASSWORD',
            payload: index
        });
        dispatch(saveBlob());
    };
}

export function saveBlob() {
    return (dispatch, getState) => {
        const state = getState();
        dispatch({
            type: 'SAVE_BLOB',
            payload: PasswordManager.save(state.main.Identity, state.main.Blob, state.main.AESKey)
        });
    };
}

export function fetchBlob(identity, AESKey) {
    return {
        type: 'FETCH_BLOB',
        payload: PasswordManager.fetch(identity, AESKey)
    };
}

export function updateSearch(q) {
    return {
        type: 'UPDATE_SEARCH',
        payload: q
    };
}

