import { createReducer } from './common.js';

const initialState = {
    emailError: '',
    passwordError: ''
};

function loginEmailError(state, action) {
    return {...state, emailError: action.payload};
}

function loginPasswordError(state, action) {
    return {...state, passwordError: action.payload};
}

function loginError(state, action) {
    return {emailError: action.payload, passwordError: action.payload};
}

export default createReducer(initialState, {
    'LOGIN_EMAIL_ERROR': loginEmailError,
    'LOGIN_PASSWORD_ERROR': loginPasswordError,
    'LOGIN_ERROR': loginError,
});
