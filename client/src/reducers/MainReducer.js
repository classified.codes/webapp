import { createReducer } from './common.js';

const initialState = {
    Blob: {
        passwords: []
    },
    Identity: null,
    AESKey: null,
    SearchString: '',
    SignedIn: false
};

function derieveAesKeyFulfilled(state, action) {
    return {...state, AESKey: action.payload};
}

function identityCheckFulfilled(state, action) {
    return {...state, Identity: action.payload, SignedIn: true};
}

function fetchBlobFulfilled(state, action) {
    return {...state, Blob: action.payload};
}

function saveBlobFulfilled(state, action) {
    return {...state, Blob: action.payload};
}

function addedPassword(state, action) {
    return {...state, Blob: {...state.Blob, passwords: [...state.Blob.passwords, action.payload]}};
}

function updatedPassword(state, action) {
    const nextPasswords = state.Blob.passwords.slice(0, action.index)
    .concat([{...action.payload}])
    .concat(state.Blob.passwords.slice(action.index + 1))
    return {...state, Blob: {...state.Blob, passwords: nextPasswords}};
}

function deletePassword(state, action) {
    const nextPasswords = state.Blob.passwords.slice(0, action.payload)
    .concat(state.Blob.passwords.slice(action.payload + 1));
    return {...state, Blob: {...state.Blob, passwords: nextPasswords}};
}

function updateSearch(state, action) {
    return {...state, SearchString: action.payload};
}

function updateSignedIn(state, action) {
    return {...state, SignedIn: action.payload};
}

function signOut(state, action) {
    return initialState
}

export default createReducer(initialState, {
    'DERIEVE_KEY_FULFILLED': derieveAesKeyFulfilled,
    'IDENTITY_CHECK_FULFILLED': identityCheckFulfilled,
    'FETCH_BLOB_FULFILLED': fetchBlobFulfilled,
    'SAVE_BLOB_FULFILLED': saveBlobFulfilled,
    'ADDED_PASSWORD': addedPassword,
    'UPDATED_PASSWORD': updatedPassword,
    'DELETE_PASSWORD': deletePassword,
    'UPDATE_SEARCH': updateSearch,
    'UPDATE_SIGNEDIN': updateSignedIn,
    'SIGN_OUT': signOut
});
