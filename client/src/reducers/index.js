import { combineReducers } from 'redux';
import LoginFormReducer from './LoginFormReducer.js';
import MainReducer from './MainReducer.js';

export default combineReducers({
    loginForm: LoginFormReducer,
    main: MainReducer
});
