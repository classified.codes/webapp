import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import {indigo, grey} from 'material-ui/colors';
import createMuiTheme from 'material-ui/styles/createMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import Router from './components/Router.jsx';
import store from './store';
import * as localStorage from './localStorage.js';
import * as Actions from './actions';
import './style.css';

const muiTheme = createMuiTheme({
    palette: {
        primary: indigo,
        secondary: grey
    }
});

if (localStorage.hasSavedCreds()) {
    store.dispatch({type: 'UPDATE_SIGNEDIN', payload: true});
    localStorage.loadCreds()
    .then(creds => {
        if (creds.key) {
            store.dispatch(Actions.Login.autoLogin(creds.email, creds.key));
        }
    });
}

ReactDOM.render((
    <Provider store={store}>
        <MuiThemeProvider theme={muiTheme}>
            <Router />
        </MuiThemeProvider>
    </Provider>
), document.getElementById('root'));
