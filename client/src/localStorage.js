import * as Crypto from './crypto/Crypto.js';

export function hasSavedCreds() {
    return !!window.localStorage.getItem('email') && !!window.localStorage.getItem('key');
}

export function loadCreds() {
    return new Promise((resolve, reject) => {
        let storedKey = window.localStorage.getItem('key');
        let email = window.localStorage.getItem('email');
        if (storedKey) {
            let keyObj = JSON.parse(storedKey);
            Crypto.importAESKey(keyObj)
            .then(key => {
                resolve({email, key});
            });
        } else {
            resolve();
        }
    });
}

export function saveCreds(email, key) {
    window.crypto.subtle.exportKey("jwk", key)
    .then(exportedKey => {
        window.localStorage.setItem('email', email);
        window.localStorage.setItem('key', JSON.stringify(exportedKey));
    });
}

export function clear() {
    window.localStorage.removeItem('email');
    window.localStorage.removeItem('key');
}
