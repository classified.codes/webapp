import React from 'react';

import {connect} from 'react-redux';
import {Link, withRouter} from 'react-router-dom';
import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import Card, { CardContent } from 'material-ui/Card';
import { FormControlLabel, FormControl, FormHelperText } from 'material-ui/Form';
import Checkbox from 'material-ui/Checkbox';
import Typography from 'material-ui/Typography';
import TextField from 'material-ui/TextField';
import SmallCard from './SmallCard.jsx';
import * as Actions from '../actions';
import * as localStorage from '../localStorage.js';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {email: '', password: '', trusted: false};
    }

    login(event) {
            event.preventDefault();
            localStorage.clear();
        this.props.dispatch(Actions.Login.login(this.state.email.value, this.state.password.value))
        .then(() => {
            if (this.state.trusted && this.state.trusted.value) {
                localStorage.saveCreds(this.state.email.value, this.props.AESKey);
            }
            this.props.history.replace('/');
        })
        .catch(e => {
            console.log(e);
        });
    }

    handleChange(event) {
        const target = event.target;
        const name = target.name;
        let value = target.value;
        if (name === 'trusted') {
            value = target.checked;
        }
        this.state[name] = {value};
    }

    render() {
        return (
            <SmallCard title="Login">
                <form onSubmit={this.login.bind(this)}>
                    <TextField
                        name="email"
                        label="Email"
                        type="email"
                        autoFocus={true}
                        fullWidth={true}
                        style={{marginBottom: '30px'}}
                        onChange={this.handleChange.bind(this)}
                        error={this.props.loginErrors.emailError !== ''}
                        helperText={this.props.loginErrors.emailError}
                    />
                    <TextField
                        name="password"
                        label="Passphrase"
                        type="password"
                        fullWidth={true}
                        style={{marginBottom: '30px'}}
                        onChange={this.handleChange.bind(this)}
                        error={this.props.loginErrors.passwordError !== ''}
                        helperText={this.props.loginErrors.passwordError }
                    />
                    <Grid container justify="space-between" alignItems="center">
                        <Grid item>
                            <Button to="/register" component={props => <Link {...props}/>}>Register</Button>
                        </Grid>
                        <Grid item className="text-right">
                            <Grid container alignItems="center">
                            <FormControlLabel
                                control={
                                    <Checkbox name="trusted" onChange={this.handleChange.bind(this)} />
                                }
                                label="Trust Device"
                            />
                            <Button raised={true} color="primary" type="submit">Login</Button>
                            </Grid>
                        </Grid>
                    </Grid>
                </form>
            </SmallCard>
        );
    }
}

export default connect(state => ({loginErrors: state.loginForm, AESKey: state.main.AESKey}))(withRouter(Login));
