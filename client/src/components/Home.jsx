import React from 'react';
import {connect} from 'react-redux';
import Grid from 'material-ui/Grid';
import Typography from 'material-ui/Typography';
import Toolbar from 'material-ui/Toolbar';
import Paper from 'material-ui/Paper';
import AddPassword from './AddPassword.jsx';
import PasswordList from './PasswordList.jsx';
import TextField from 'material-ui/TextField';
import * as Actions from '../actions';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {editing: false, editIndex: null, filter: ''};
    }

    handleNewPassword(password) {
        this.props.dispatch(Actions.Account.addPassword(password));
    }

    render() {
        return (
            <div style={{flexGrow: 1, margin: '0 20px'}}>
                <Grid container>
                    <Grid item xs={12}>
                        <Paper>
                            <TableToolbar />
                            <PasswordList filter={this.props.q} passwords={this.props.passwords} onDelete={this.handleDelete.bind(this)} onEdit={this.handleEdit.bind(this)} />
                        </Paper>
                    </Grid>
                </Grid>
                <AddPassword onSubmit={this.handleNewPassword.bind(this)} />
            </div>
        );
    }

    handleFilterChange(event) {
        this.setState({filter: event.target.value});
    }
    
    handleDelete(index) {
        this.props.dispatch(Actions.Account.deletePassword(index));
    }

    handleEdit(password, index) {
        this.props.dispatch(Actions.Account.updatePassword(password, index));
    }
}

let TableToolbar = props => {
    return (
        <Toolbar>
            <div><Typography type="title">Passwords</Typography></div>
        </Toolbar>
    );
};


export default connect(state => ({passwords: state.main.Blob.passwords, q: state.main.SearchString}))(Home);
