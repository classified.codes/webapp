import React from 'react';
import {connect} from 'react-redux';
import { Route, withRouter } from 'react-router-dom';
import {indigo} from 'material-ui/colors';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import TextField from 'material-ui/TextField';
import AppBarSearch from './AppBarSearch.jsx';
import Hidden from 'material-ui/Hidden';
import Grid from 'material-ui/Grid';
import Icon from 'material-ui/Icon';
import IconButton from 'material-ui/IconButton';
import { AuthenticatedRoute, AuthenticatorRoute } from './AuthenticatedRoute.jsx';
import * as Actions from '../actions';

import Login from './Login.jsx';
import Home from './Home.jsx';
import Register from './Register.jsx';

class App extends React.Component {
    render() {
        const searching = this.props.q !== '';
        return (
            <div>
                <AppBar
                    position="static"
                    style={{
                        marginBottom: '30px',
                        backgroundColor: searching ? indigo[100] : indigo[500],
                        color: searching ? '#333333' : 'white'
                    }}>
                    <Toolbar>
                        <Grid container justify="space-between" alignItems="center" spacing={0}>
                            {this.renderTitle(searching)}
                            <Grid item xs={1} sm={6} >
                                <Hidden smDown implementation="css">
                                    <AuthenticatedRoute exact path="/" component={AppBarSearch} />
                                </Hidden>
                            </Grid>
                            {this.renderRightMenu(searching)}
                        </Grid>
                    </Toolbar>
				</AppBar>
                <AuthenticatedRoute exact path="/" component={Home} />
                <AuthenticatorRoute path="/login" component={Login} />
                <Route path="/register" component={Register} />
            </div>
        );
    }

    renderTitle(searching) {
        if (searching) {
            return (
                <Grid item xs={6} sm={3}>
                    <IconButton style={{marginLeft: '20px'}} onClick={this.handleClearSearch.bind(this)}>
                        <Icon>arrow_back</Icon> 
                        <Typography type="title" color="inherit" style={{display: 'inline-block', marginLeft: '20px'}}>
                            Back
                        </Typography>
                    </IconButton>
                </Grid>
            );
        }
        return (
            <Grid item xs={6} sm={3}>
                <Typography type="title" color="inherit">
                    classified.codes
                </Typography>
            </Grid>
        );
    }

    renderRightMenu(searching) {
        return (
            <Grid item xs={5} sm={3}>
                <AuthenticatedRoute exact path="/" component={() => (
                    <Typography className="clicky" type="title" color="inherit" align="right" onClick={this.handleSignOut.bind(this)}>
                        {searching ? '' : 'bye'}
                    </Typography>
                )} />
            </Grid>
        );
    }

    handleClearSearch() {
        this.props.dispatch(Actions.account.updateSearch(''));
    }

    handleSignOut() {
        this.props.dispatch(Actions.Login.signOut());
        this.props.history.replace('/login');
    }
}

export default connect(state => ({q: state.main.SearchString}))(withRouter(App));
