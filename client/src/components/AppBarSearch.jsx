import React from 'react';
import {connect} from 'react-redux';
import Icon from 'material-ui/Icon';
import * as Actions from '../actions';

class AppBarSearch extends React.Component {
    render() {
        return (
            <div className="appbar-search">
                <div className="appbar-search-icon">
                    <Icon>search</Icon>
                </div>
                <span>
                    <input
                        value={this.props.q}
                        placeholder="Search"
                        ref={input => this.searchInput = input}
                        className="appbar-search-input"
                        autoComplete="off"
                        spellCheck="false"
                        onChange={this.handleChange.bind(this)}
                    />
                </span>
            </div>
        );
    }

    handleChange(event) {
        this.props.dispatch(Actions.Account.updateSearch(event.target.value));
    }

    handleKeyDown(event) {
        if (event.keyCode == 83 && event.altKey) {
            this.searchInput.focus();
            this.props.dispatch(Actions.Account.updateSearch(''));
            event.preventDefault();
        }
    }

    componentWillMount() {
        window.document.addEventListener('keydown', this.handleKeyDown.bind(this));
    }

    componentWillUnmount() {
        window.document.removeEventListener('keydown', this.handleKeyDown.bind(this));
    }
}

export default connect(state => ({q: state.main.SearchString}))(AppBarSearch);
