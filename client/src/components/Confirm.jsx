import React from 'react';
import Button from 'material-ui/Button';
import Dialog, {DialogActions, DialogContent, DialogContentText, DialogTitle} from 'material-ui/Dialog';
import Typography from 'material-ui/Typography';

export default class AddPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {open: false};
    }

    render() {
        return (
            <div>
                <Dialog maxWidth="md" open={this.state.open} onRequestClose={this.handleClose.bind(this)}>
                    <DialogTitle>{this.props.title}</DialogTitle>
                    <DialogContent>
                        <Typography type="body1">{this.props.message}</Typography>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose.bind(this)} color="primary">Cancel</Button>
                        <Button type="submit" color="primary" onClick={this.handleAccept.bind(this)}>Confirm</Button>
                    </DialogActions>
                </Dialog>
                <div onClick={this.handleOpen.bind(this)}>{this.props.children}</div>
            </div>
        );
    }

    handleOpen() {
        this.setState({open: true});
    }

    handleClose() {
        this.setState({open: false});
    }

    handleAccept() {
        this.setState({open: false});
        this.props.onAccept();
    }
}
