import React from 'react';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import Typography from 'material-ui/Typography';
import CopyMe from './CopyMe.jsx';
import Icon from 'material-ui/Icon';
import Confirm from './Confirm.jsx';
import PasswordEditor from './PasswordEditor.jsx';

export default class PasswordList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {editing: false, editIndex: null, passwords: this.filterPasswords(this.props.filter, this.props.passwords)};
    }

    componentWillReceiveProps(nextProps) {
        this.setState({passwords: this.filterPasswords(nextProps.filter, nextProps.passwords)});
    }

    filterPasswords(filter, passwords) {
        if (filter.trim() === '') {
            return passwords;
        }

        let filteredResult = passwords.filter(password => {
            const searchString = password.name + password.context.join('') + password.username;
            return searchString.toLowerCase().search(filter.toLowerCase()) !== -1;
        });
        return filteredResult;
    }

    render() {
        return (
            <div style={{flexGrow: 1, margin: '0 20px', overflowX: 'auto'}}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Website</TableCell>
                            <TableCell>Username</TableCell>
                            <TableCell>Password</TableCell>
                            <TableCell style={{width: '1rem'}}>Edit</TableCell>
                            <TableCell style={{width: '1rem'}}>Delete</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.renderRows()}
                    </TableBody>
                </Table>
                <PasswordEditor
                    open={this.state.editing}
                    password={this.state.passwords[this.state.editIndex]}
                    onRequestClose={this.handleEditClose.bind(this)}
                    onSubmit={this.handleEditSubmit.bind(this)}
                    editMode={true}
                />
            </div>
        );
    }
    
    renderRows() {
        return this.state.passwords.map((record, index) => {
            return (
                <TableRow key={record.name}>
                    <TableCell>{record.name}</TableCell>
                    <TableCell><CopyMe>{record.username}</CopyMe></TableCell>
                    <TableCell><CopyMe private={true}>{record.password}</CopyMe></TableCell>
                    <TableCell><Icon color="action" className="clicky" onClick={() => this.edit(index)}>edit</Icon></TableCell>
                    <TableCell>
                        <Confirm title="Delete Password" message="Are you sure you want to delete this password?" onAccept={() => this.delete(index)}>
                            <Icon color="action" className="clicky">delete</Icon>
                        </Confirm>
                    </TableCell>
                </TableRow>
            );
        });
    }

    renderName(record) {
        return (
            <span>
                {record.name}
                <Typography style={{display: 'inline-block'}} color="secondary">
                    [{record.context.map(domain => (<span>{domain}, </span>))}]
                </Typography>
            </span>
        );
    }

    edit(index) {
        this.setState({editing: true, editIndex: index});
    }

    delete(index) {
        this.props.onDelete(index);
    }

    handleEditClose() {
        this.setState({editing: false, editIndex: null});
    }

    handleEditSubmit(password) {
        this.setState({editing: false, editIndex: null});
        this.props.onEdit(password, this.state.editIndex);
    }
}

PasswordList.defaultProps = {
    filter: '',
    passwords: []
};
