import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import store from '../store.js';

function signedIn() {
    return store.getState().main.SignedIn;
}

export const AuthenticatedRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    signedIn() ? (
      <Component {...props}/>
    ) : (
      <Redirect to={{
        pathname: '/login',
        state: { from: props.location }
      }}/>
    )
  )}/>
);

export const AuthenticatorRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    signedIn() ? (
      <Redirect to={{
        pathname: '/',
        state: { from: props.location }
      }}/>
    ) : (
      <Component {...props}/>
    )
  )}/>
);
