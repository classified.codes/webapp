import React from 'react';
import * as Utils from '../crypto/Utils.js';

export default class CopyMe extends React.Component {
    constructor(props) {
        super(props);
        this.state = {copying: false, hovering: false};
    }

    handleCopy(text) {
        Utils.copyToClipboard(text);
        this.setState({copying: true});
        setTimeout(() => this.setState({copying: false}), 500);
    }

    handleMouseEnter() {
        this.setState({hovering: true});
    }

    handleMouseOut() {
        this.setState({hovering: false});
    }

    render() {
        return (
            <span
                className={`click-to-copy${this.state.copying ? ' copying' : ''}`}
                onClick={this.handleCopy.bind(this, this.props.children)}
                onMouseOver={this.handleMouseEnter.bind(this)}
                onMouseOut={this.handleMouseOut.bind(this)}>
                <span className={`click-to-copy-text${this.props.private ? ' private-text' : ''}`}>
                    {this.props.children}
                </span>
                <span data-label="Copied" className="click-to-copy-label"> Copy </span>
            </span>
        );
    }
}

CopyMe.defaultProps = {
    private: false
};
