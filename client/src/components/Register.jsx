import React from 'react';
import {connect} from 'react-redux';
import {Link, withRouter} from 'react-router-dom';

import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import Card, { CardContent } from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import TextField from 'material-ui/TextField';
import SmallCard from './SmallCard.jsx';
import * as localStorage from '../localStorage.js';

class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    register(event) {
        event.preventDefault();
        this.props.dispatch(register(this.state.email, this.state.password))
        .then(() => {
            //localStorage.saveIdentity(this.props.Identity);
            this.props.history.push('/');
        });
    }

    handleChange(event) {
        const target = event.target;
        const name = target.id;
        const value = target.value;
        this.state[name] = value;
    }

    render() {
        return (
            <SmallCard title="Register">
                <form onSubmit={this.register.bind(this)}>
                    <div>
                        <TextField
                            id="email"
                            label="Email"
                            type="email"
                            autoFocus={true}
                            fullWidth={true}
                            style={{marginBottom: '30px'}}
                            onChange={this.handleChange.bind(this)}
                        />
                    </div>
                    <div>
                        <TextField
                            id="password"
                            label="Passphrase"
                            type="password"
                            fullWidth={true}
                            style={{marginBottom: '30px'}}
                            onChange={this.handleChange.bind(this)}
                        />
                    </div>
                    <div>
                        <TextField
                            id="password-confirm"
                            label="Confirm Passphrase"
                            type="password"
                            fullWidth={true}
                            style={{marginBottom: '30px'}}
                            onChange={this.handleChange.bind(this)}
                        />
                    </div>
                    <Grid container justify="space-between">
                        <Grid item>
                            <Button to="/login" component={props => <Link {...props}/>}>Login</Button>
                        </Grid>
                        <Grid item>
                            <Button raised={true} color="primary" type="submit">Register</Button>
                        </Grid>
                    </Grid>
                </form>
            </SmallCard>
        );
    }
}

export default connect(state => ({Identity: state.main.Identity}))(withRouter(Register));
