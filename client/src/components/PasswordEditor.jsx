import axios from "axios";
import React from 'react';
import Grid from 'material-ui/Grid';
import Button from 'material-ui/Button';
import Menu, { MenuItem } from 'material-ui/Menu';
import Icon from 'material-ui/Icon';
import Dialog, {DialogActions, DialogContent, DialogContentText, DialogTitle, withMobileDialog} from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';

class PasswordEditor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {generateMenuOpen: false};
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.open) {
            this.setState({...nextProps.password, context: nextProps.password.context[0], generateOpenMenu: false});
        }
    }

    render() {
        return (
            <Dialog fullScreen={this.props.fullScreen} maxWidth="md" open={this.props.open} onRequestClose={this.props.onRequestClose}>
                <DialogTitle>{this.props.editMode ? 'Edit' : 'Add'} Password</DialogTitle>
                <form onSubmit={this.handleFormSubmit.bind(this)}>
                    <DialogContent>
                        {this.renderForm()}
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.props.onRequestClose} color="primary">Cancel</Button>
                        <Button type="submit" color="primary">Save</Button>
                    </DialogActions>
                </form>
            </Dialog>
        );
    }

    renderForm() {
        return (
            <div>
                <TextField
                    autoFocus
                    id="name"
                    label="Name"
                    type="text"
                    margin="normal"
                    onChange={this.handleFormChange.bind(this)}
                    value={this.state.name}
                    fullWidth
                />
                <TextField
                    id="context"
                    label="Context"
                    type="text"
                    margin="normal"
                    onChange={this.handleFormChange.bind(this)}
                    value={this.state.context}
                    fullWidth
                />
                <TextField
                    id="username"
                    label="Username"
                    type="text"
                    margin="normal"
                    onChange={this.handleFormChange.bind(this)}
                    value={this.state.username}
                    fullWidth
                />
                <Grid container alignItems="flex-end" spacing={0}>
                    <Grid item xs={8} sm={9}>
                        <TextField
                            id="password"
                            label="Password"
                            type="text"
                            margin="normal"
                            onChange={this.handleFormChange.bind(this)}
                            value={this.state.password}
                            fullWidth
                        />
                    </Grid>
                    <Grid item xs={4} sm={3} style={{textAlign: 'center'}}>
                        <Button style={{marginBottom: '7px'}} onClick={this.handleToggleGenerateMenu.bind(this)}>Generate<Icon>arrow_drop_down</Icon></Button>
                        <Menu anchorEl={this.state.generateButton} open={this.state.generateMenuOpen} onRequestClose={this.handleRequestCloseGeneratorMenu.bind(this)}>
                            <MenuItem onClick={() => this.handleGeneratePassword('complex')}><Icon>lock</Icon> Complex</MenuItem>
                            <MenuItem onClick={() => this.handleGeneratePassword('compatible')}><Icon>account_balance</Icon> Compatible</MenuItem>
                            <MenuItem onClick={() => this.handleGeneratePassword('human')}><Icon>person</Icon> Human</MenuItem>
                        </Menu>
                    </Grid>
                </Grid>
            </div>
        );
    }

    handleToggleGenerateMenu(event) {
        this.setState({generateMenuOpen: !this.state.generateMenuOpen, generateButton: event.currentTarget});
    }

    handleRequestCloseGeneratorMenu() {
        this.setState({generateMenuOpen: false});
    }

    handleGeneratePassword(type) {
        this.randomPassword(type)
        .then(password => {
            this.setState({password: password, generateMenuOpen: false});
        });
    }

    handleFormChange(event) {
        const target = event.target;
        const name = target.id;
        const value = target.value;
        let newState = {};
        newState[name] = value;
        this.setState(newState); 
    }

    handleFormSubmit(event) {
        event.preventDefault();
        const password = {
            name: this.state.name,
            context: [this.state.context],
            username: this.state.username,
            password: this.state.password
        }
        this.props.onSubmit(password);
    }

    randomPassword(type) { 
        return new Promise((resolve, reject) => {
            if (type === 'complex') {
                const complex = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()-=_+[]{}}|;:/?.>,<`~";
                return resolve(this.randomString(complex, 20));
            }
            if (type === 'compatible') {
                const compatible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%";
                return resolve(this.randomString(compatible, 12));
            }
            if (type === 'human') {
                return axios.get('/api/pass-phrase')
                .then(response => {
                    return resolve(response.data['passPhrase']);
                })
            }
            return '';
        });
    }

    randomString(characterSet, length) {
        let result = '';
        for (let i = 0; i < length; i++) {
            result += characterSet.charAt(Math.floor(Math.random() * characterSet.length));
        }
        return result;
    }
}

PasswordEditor.defaultProps = {
    password: {
        name: '',
        username: '',
        context: [''],
        password: ''
    },
    editMode: false
}

export default withMobileDialog()(PasswordEditor);
