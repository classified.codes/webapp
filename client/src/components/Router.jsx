import React from 'react';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';

import App from './App.jsx';

export default class Router extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <Route path="/" component={App} />
            </BrowserRouter>
        );
    }
}
