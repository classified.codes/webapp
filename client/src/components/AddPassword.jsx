import React from 'react';
import AddIcon from 'material-ui-icons/Add';
import Button from 'material-ui/Button';
import Tooltip from 'material-ui/Tooltip';
import PasswordEditor from './PasswordEditor.jsx'

export default class AddPassword extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div>
                <PasswordEditor open={this.state.open} onSubmit={this.handleSubmit.bind(this)} onRequestClose={this.handleRequestclose.bind(this)}/>
                <Tooltip placement="top-start" title="Add">
                    <Button fab color="primary" aria-label="add" id="add-button" onClick={this.handleAdd.bind(this)}><AddIcon /></Button>
                </Tooltip>
            </div>
        );
    }

    handleAdd() {
        this.setState({open: true});
    }

    handleKeyDown(event) {
        if (event.keyCode == 65 && event.altKey) {
            this.setState({open: true});
            event.preventDefault();
        }
    }

    handleSubmit(password) {
        this.setState({open: false});
        this.props.onSubmit(password);
    }

    handleRequestclose(event) {
        this.setState({open: false});
    }

    componentWillMount() {
        window.document.addEventListener('keydown', this.handleKeyDown.bind(this));
    }

    componentWillUnmount() {
        window.document.removeEventListener('keydown', this.handleKeyDown.bind(this));
    }
}
