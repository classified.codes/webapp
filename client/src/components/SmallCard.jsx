import React from 'react';

import Grid from 'material-ui/Grid';
import Card, { CardContent } from 'material-ui/Card';
import Typography from 'material-ui/Typography';

export default class SmallCard extends React.Component {
    render() {
        let title = this.props.title;
        return (
            <Grid container justify="center" spacing={0}>
                <Grid item xs={12} sm={8} md={6} lg={4}>
                    <Card>
                        <CardContent>
                            <Typography type="title" style={{marginBottom: '30px'}}>{title}</Typography>
                            { this.props.children }
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        );
    }
}
