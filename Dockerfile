FROM golang:1.8 AS go-builder
WORKDIR /usr/src/classified
COPY ./server .
RUN go-wrapper download
RUN go build -v

FROM registry.gitlab.com/findley/node-go AS node-builder
WORKDIR /root/classified
COPY ./client .
RUN npm set progress=false && npm config set depth 0
RUN npm install
RUN npm run build

FROM registry.gitlab.com/findley/node-go
WORKDIR /
COPY --from=go-builder /usr/src/classified/classified /usr/local/bin
COPY --from=node-builder /root/classified/dist /var/www/classified.codes
EXPOSE 80 443
CMD ["/usr/local/bin/classified"]
