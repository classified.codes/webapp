package main

import (
	"bufio"
	"crypto"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/hex"
	"encoding/json"
	"encoding/pem"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"path"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"github.com/mendsley/gojwk"
	yaml "gopkg.in/yaml.v2"
)

var configFile string
var config *Config
var dict []string

type RegisterRequest struct {
	Account      string    `json:"account"`
	Username     string    `json:"username"`
	PublicKey    gojwk.Key `json:"publicKey"`
	AccountToken string    `json:"accountToken"`
	Signature    string    `json:"signature"`
}

type IdentityTestRequest struct {
	Username string `json:"username"`
}

type IdentityTestResponse struct {
	IdentityTestToken string `json:"identityTestToken"`
	Account           string `json:"account"`
}

type UserIdentity struct {
	Username          string `json:"username"`
	AccountToken      string `json:"accountToken"`
	IdentityTestToken string `json:"identityTestToken"`
	Signature         string `json:"signature"`
}

type GetBlobRequest struct {
	Identity UserIdentity `json:"identity"`
}

type GetBlobResponse struct {
	Blob string `json:"blob"`
}

type GetPassPhraseResponse struct {
	PassPhrase string `json:"passPhrase"`
}

type SaveBlobRequest struct {
	Identity UserIdentity `json:"identity"`
	Blob     string       `json:"blob"`
}

type Config struct {
	Port         int    `yaml:"Port"`
	HttpRedirect bool   `yaml:"HttpRedirect"`
	CertFile     string `yaml:"CertFile"`
	KeyFile      string `yaml:"KeyFile"`
	StaticDir    string `yaml:"StaticDir"`
	DataDir      string `yaml:"DataDir"`
	IndexFile    string `yaml:"IndexFile"`
	TokenSecret  string `yaml:"TokenSecret"`
}

func init() {
	flag.StringVar(&configFile, "c", "/etc/classified.codes/config.yml", "path to config file")
}

func loadConfig(file string) (*Config, error) {
	var result Config
	fileBytes, err := ioutil.ReadFile(file)
	if err != nil {
        log.Printf("Could not read config file: %v Falling back to ./config.yml", file)
        fileBytes, err = ioutil.ReadFile("./config.yml")
        if err != nil {
            return nil, fmt.Errorf("Could find a config file", err)
        }
	}
	err = yaml.Unmarshal(fileBytes, &result)
	if err != nil {
		return nil, fmt.Errorf("Could not parse config file: %v", err)
	}
	return &result, nil
}

func main() {
	flag.Parse()
	var err error
	config, err = loadConfig(configFile)
	if err != nil {
		log.Fatal(err)
	}
	dict, err = readDictionary()
	if err != nil {
		log.Fatal(err)
	}
	setupDataDir()
	r := mux.NewRouter()
	r.HandleFunc("/api/account-token", accountTokenHandler)
	r.HandleFunc("/api/register", registerHandler)
	r.HandleFunc("/api/identity-test", identityTestHandler)
	r.HandleFunc("/api/save-blob", saveBlobHandler)
	r.HandleFunc("/api/fetch-blob", getBlobHandler)
	r.HandleFunc("/api/pass-phrase", getPassPhraseHandler)
	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(config.StaticDir))))
	r.PathPrefix("/").HandlerFunc(IndexHandler(config.IndexFile))
	http.Handle("/", r)
	log.Printf("Server running on port %d\n", config.Port)

	if config.HttpRedirect {
		go http.ListenAndServe(":80", http.HandlerFunc(redirect))
	}
	log.Fatal(http.ListenAndServeTLS(fmt.Sprintf(":%d", config.Port), config.CertFile, config.KeyFile, nil))
}

func setupDataDir() {
	_ = os.Mkdir(path.Join(config.DataDir, "blobs"), 0755)
	_ = os.Mkdir(path.Join(config.DataDir, "public_keys"), 0755)
	_ = os.Mkdir(path.Join(config.DataDir, "accounts"), 0755)
}

func redirect(w http.ResponseWriter, req *http.Request) {
	target := "https://" + req.Host + req.URL.Path
	if len(req.URL.RawQuery) > 0 {
		target += "?" + req.URL.RawQuery
	}
	http.Redirect(w, req, target, http.StatusTemporaryRedirect)
}

func IndexHandler(entrypoint string) func(w http.ResponseWriter, r *http.Request) {
	fn := func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, entrypoint)
	}

	return http.HandlerFunc(fn)
}

func accountTokenHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Method not allowed", 405)
		return
	}
	token, err := makeAccountToken()
	if err != nil {
		http.Error(w, "Failed to create account token", 500)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, "{\"token\": \""+token+"\"}")
}

func registerHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Method not allowed", 405)
		return
	}

	// Check size of request is not too big

	decoder := json.NewDecoder(r.Body)
	var registerRequest RegisterRequest
	if err := decoder.Decode(&registerRequest); err != nil {
		http.Error(w, "Could not read request body", 400)
		return
	}

	if accountExists(registerRequest.Username) {
		http.Error(w, "Account already exists", 400)
		return
	}

	accountToken, err := parseToken(registerRequest.AccountToken)
	if err != nil {
		http.Error(w, "Could not parse account token", 400)
		return
	}
	if !accountToken.Valid {
		http.Error(w, "Invalid account token", 400)
		return
	}
	publicKey, err := registerRequest.PublicKey.DecodePublicKey()
	if err != nil {
		http.Error(w, "Could not decode public key", 400)
		return
	}
	rsaPublicKey, ok := publicKey.(*rsa.PublicKey)
	if !ok {
		http.Error(w, "Public key was not an RSA key", 400)
		return
	}
	if !verifyRSASignature(registerRequest.AccountToken, registerRequest.Signature, rsaPublicKey) {
		http.Error(w, "Invalid RSA signature on account token", 400)
		return
	}

	err = writeAccountToFile(registerRequest.Account, registerRequest.Username)
	if err != nil {
		log.Println(err)
		http.Error(w, "Failed to create account.", 500)
		return
	}

	err = savePublicPEMKey(registerRequest.PublicKey, registerRequest.Username)
	if err != nil {
		log.Println(err)
		http.Error(w, "Failed to create account.", 500)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, "{\"success\": true}")
}

func identityTestHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Method not allowed", 405)
		return
	}

	decoder := json.NewDecoder(r.Body)
	var identityTestRequest IdentityTestRequest
	if err := decoder.Decode(&identityTestRequest); err != nil {
		http.Error(w, "Could not read request body", 400)
		return
	}

	idToken, err := makeIdToken()
	if err != nil {
		http.Error(w, "Could not get ID test", 500)
		return
	}

	account, err := loadAccount(identityTestRequest.Username)
	if err != nil {
		http.Error(w, "No account exists", 400)
		return
	}

	response := &IdentityTestResponse{
		IdentityTestToken: idToken,
		Account:           account,
	}

	responseBytes, err := json.Marshal(response)
	if err != nil {
		http.Error(w, "Could not send response", 500)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(responseBytes)
}

func saveBlobHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Method not allowed", 405)
		return
	}

	decoder := json.NewDecoder(r.Body)
	var saveBlobRequest SaveBlobRequest
	if err := decoder.Decode(&saveBlobRequest); err != nil {
		http.Error(w, "Could not read request body", 400)
		return
	}

	accountId, ok := verifyIdentity(&saveBlobRequest.Identity)
	if !ok {
		http.Error(w, "Could not verify identity", 400)
		return
	}

	err := ioutil.WriteFile(path.Join(config.DataDir, "blobs", accountId), []byte(saveBlobRequest.Blob), 0644)
	if err != nil {
		http.Error(w, "Failed to save blob", 500)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, "{\"success\": true}")
}

func getPassPhraseHandler(w http.ResponseWriter, r *http.Request) {
	result := makePhrase(15)

	response := &GetPassPhraseResponse{
		PassPhrase: result,
	}

	responseBytes, err := json.Marshal(response)
	if err != nil {
		http.Error(w, "Could not send response", 500)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(responseBytes)
}

func getBlobHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Method not allowed", 405)
		return
	}

	decoder := json.NewDecoder(r.Body)
	var getBlobRequest GetBlobRequest
	if err := decoder.Decode(&getBlobRequest); err != nil {
		http.Error(w, "Could not read request body", 400)
		return
	}

	accountId, ok := verifyIdentity(&getBlobRequest.Identity)
	if !ok {
		http.Error(w, "Could not verify identity", 400)
		return
	}

	content, err := ioutil.ReadFile(path.Join(config.DataDir, "blobs", accountId))
	if err != nil {
		http.Error(w, "Could not load blob", 500)
		return
	}

	response := &GetBlobResponse{
		Blob: string(content),
	}

	responseBytes, err := json.Marshal(response)
	if err != nil {
		http.Error(w, "Could not send response", 500)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(responseBytes)
}

func makeAccountToken() (string, error) {
	accountId, err := uniqueId()
	if err != nil {
		return "", fmt.Errorf("Could not generate accountId: %v", err)
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"accountId": accountId,
	})
	tokenString, err := token.SignedString([]byte(config.TokenSecret))
	if err != nil {
		return "", fmt.Errorf("Could not sign account token: %v", err)
	}
	return tokenString, nil
}

func makeIdToken() (string, error) {
	randomTest, err := uniqueId()
	if err != nil {
		return "", fmt.Errorf("Could not generate random value: %v", err)
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"v":   randomTest,
		"exp": time.Now().Unix() + 60*60,
	})
	tokenString, err := token.SignedString([]byte(config.TokenSecret))
	if err != nil {
		return "", fmt.Errorf("Could not sign id test token: %v", err)
	}
	return tokenString, nil
}

func parseToken(tokenString string) (*jwt.Token, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(config.TokenSecret), nil
	})
	if err != nil {
		return nil, err
	}
	return token, nil
}

func verifyIdentity(identity *UserIdentity) (accountId string, valid bool) {
	valid = false
	accountId = ""

	accountToken, err := parseToken(identity.AccountToken)
	if err != nil {
		log.Println("Could not parse account token")
		return
	}
	if claims, ok := accountToken.Claims.(jwt.MapClaims); ok && accountToken.Valid {
		accountId = claims["accountId"].(string)
	} else {
		log.Println("Invalid account token")
		return
	}

	identityToken, err := parseToken(identity.IdentityTestToken)
	if err != nil {
		log.Println("Could not parse identity token")
		return
	}
	if !identityToken.Valid {
		log.Println("Invalid identity token")
		return
	}

	publicKey, err := loadPublicPEMKey(identity.Username)
	if err != nil {
		log.Println("Could not load public key for user")
		return
	}

	if !verifyRSASignature(identity.IdentityTestToken, identity.Signature, publicKey) {
		return
	}

	valid = true
	return
}

func verifyRSASignature(document, sigString string, publicKey *rsa.PublicKey) bool {
	message := []byte(document)
	signature, err := hex.DecodeString(sigString)
	if err != nil {
		log.Printf("Could not decode signature from hex: %v\n", err)
		return false
	}
	hashedMessage := sha256.Sum256(message)
	err = rsa.VerifyPKCS1v15(publicKey, crypto.SHA256, hashedMessage[:], signature)
	if err != nil {
		log.Printf("RSA signature verification failed: %v\n", err)
		return false
	}
	return true
}

func writeAccountToFile(account, username string) error {
	err := ioutil.WriteFile(path.Join(config.DataDir, "accounts", username), []byte(account), 0644)
	if err != nil {
		return fmt.Errorf("Could not save account to file: %v", err)
	}
	return nil
}

func savePublicPEMKey(key gojwk.Key, username string) error {
	publicKey, err := key.DecodePublicKey()
	if err != nil {
		return fmt.Errorf("Could not extract public key to save it: %v", err)
	}

	asn1Bytes, err := x509.MarshalPKIXPublicKey(publicKey)
	if err != nil {
		return fmt.Errorf("Could not convert public key to ans1: %v", err)
	}

	var pemkey = &pem.Block{
		Type:  "RSA PUBLIC KEY",
		Bytes: asn1Bytes,
	}

	pemfile, err := os.Create(path.Join(config.DataDir, "public_keys", username))
	if err != nil {
		return fmt.Errorf("Could not open public key file: %v", err)
	}
	defer pemfile.Close()

	err = pem.Encode(pemfile, pemkey)
	if err != nil {
		return fmt.Errorf("Could not write public key to file: %v", err)
	}
	return nil
}

func loadPublicPEMKey(username string) (*rsa.PublicKey, error) {
	content, err := ioutil.ReadFile(path.Join(config.DataDir, "public_keys", username))
	if err != nil {
		return nil, fmt.Errorf("Could not read public key file: %v", err)
	}
	block, _ := pem.Decode(content)
	if block == nil || block.Type != "RSA PUBLIC KEY" {
		return nil, fmt.Errorf("No valid public key could be loaded")
	}
	key, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, fmt.Errorf("Failed to decode public key: %v", err)
	}
	rsaKey, ok := key.(*rsa.PublicKey)
	if !ok {
		return nil, fmt.Errorf("The public key was not an RSA key")
	}
	return rsaKey, nil
}

func accountExists(username string) bool {
	if _, err := os.Stat(path.Join(config.DataDir, "accounts", username)); os.IsNotExist(err) {
		return false
	}
	return true
}

func loadAccount(username string) (string, error) {
	content, err := ioutil.ReadFile(path.Join(config.DataDir, "accounts", username))
	if err != nil {
		return "", fmt.Errorf("Could not read account file: %v", err)
	}
	return string(content), nil
}

func uniqueId() (string, error) {
	b := make([]byte, 16)
	_, err := rand.Read(b)
	if err != nil {
		return "", fmt.Errorf("Could not read random bytes: %v", err)
	}
	return fmt.Sprintf("%X-%X-%X-%X-%X", b[0:4], b[4:6], b[6:8], b[8:10], b[10:]), nil
}

func generateIdTest() ([]byte, error) {
	b := make([]byte, 8)
	_, err := rand.Read(b)
	if err != nil {
		return nil, fmt.Errorf("Could not read random bytes: %v", err)
	}
	return b, nil
}

func makePhrase(maxLength int) string {
	result := ""
	for len(result) < maxLength {
		index := int(rand.Float64() * float64(len(dict)))
		if len(result) > 0 {
			result += " "
		}
		result += dict[index]
	}

	return strings.Replace(strings.ToLower(result), "'", "", -1)
}

func readDictionary() ([]string, error) {
	return readFileLines("/usr/share/dict/words")
}

func readFileLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}
